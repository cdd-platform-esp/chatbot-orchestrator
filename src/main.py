import json 
import requests
from flask import Flask
from flask import request
app = Flask(__name__)

#change this!
FORWARD_URL = 'http://production.34.91.48.40.nip.io/webhook'

@app.route('/webhook', methods = ['GET', 'POST'])
def get_data():
    if request.method == 'GET':
        print("nop")
        return requests.get(FORWARD_URL).content
    
    if request.method == 'POST':
        original_data = request.json # a multidict containing POST data
        action_payload = requests.post(FORWARD_URL,  json = dict(original_data)).content
        payload_response = json.loads(action_payload)
        return payload_response

# used for debug
@app.route('/health', methods = ['GET', 'POST'])
def show_data():
    if request.method == 'GET':
        return 'Forwarded-GET'
    
    if request.method == 'POST':
        return 'Forwarded-POST'



if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5055')
