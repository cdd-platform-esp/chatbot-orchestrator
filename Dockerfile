FROM tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7
COPY ./src/main.py /app/main.py
COPY  ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt 
ENTRYPOINT [ "python" ]
CMD [ "main.py" ]
